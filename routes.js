var users = require('./models/users');
var articles = require('./models/articles');
 
module.exports = {
  configure: function(app) {
    app.get('/users/', function(req, res) {
      users.get(res);
    });

    app.get('/user/:id/articles', function(req, res) {
      articles.get(req.params.id, res);
    });
 
    app.post('/user/', function(req, res) {
      users.create(req.body, res);
    });

    app.post('/user/article', function(req, res) {
      articles.create(req.body, res);
    });
  }
};