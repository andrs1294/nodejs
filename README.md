# NodeJs - Backend

Crear un API REST que tenga las siguientes caracteristicas. (toda la prueba es backend no tiene aspeco visual.)

Un usuario posee una lista de aticulos:

1. listar todos los usuarios, de manera paginada. (GET)
2. listar todos los articulos por un usuario especifico. (GET)
3. crear un usuario. (POST)
4. crear un articulo asociado a un usario. (POST)
5. Usar una base de datos MYSQL

### Instalación

Require de [Node.js](https://nodejs.org/) v4+ para ejecutarse.
Cargue el Script SQL llamado nodejs.sql en su servidor MySQL.

Instale las dependencias e inicie el servidor.

```sh
$ npm install -d
$ node app
```
### Routes
* /users/              - ** Lista todos los usuarios (GET) **
* /user/1/articles     - **Lista los artículos por el id del usuario (GET) **
* /user/               - **Crea un usuario (POST)**
* /user/article        - **Crea un artículo para un usuario (POST)**

### Plugin
Se recomienda el uso del plugin Advanced Rest Client para el envio y recepción de información en Chrome.

###Nota
Para realizar una inserción, los campos del formulario deben tener el mismo nombre que los atributos de la tabla de la base de datos. El campo Id no es necesario incluirlo. Un ejemplo puede ser encontrado dentro de la carpeta images

**Andrés Mauricio Gómez Peña**
[BitBucket](https://bitbucket.org/andrs1294)
[GitHub](https://github.com/andrs1294)