var connection = require('../connection');
var utils = require('./utils');

function User() {

	this.get = function(res) {
		connection.acquire(function(err, con) {
			con.query('select * from users', function(err, result) {
				con.release();
				//res.json(200,result);
				var jsonString = utils.addData(result);
				res.send(JSON.parse(jsonString));
			});
		});
	};


	this.create = function(user, res) {
		connection.acquire(function(err, con) {
			con.query('insert into users set ?', user, function(err, result) {
				con.release();
				if (err) {
					res.send({status: 1, message: 'User creation failed'});
				} else {
					res.send({status: 0, message: 'User created successfully'});
				}
			});
		});
	};

}

/*
function getCountUsers()
{
	connection.acquire(function(err, con) {
		con.query('SELECT COUNT(1) as count FROM users;', function(err, result) {
			con.release();
			if (err) {
				return -1;
			} else {
				return result[0].count;
			}
		});
	});
}

*/

module.exports = new User();