module.exports = 
{

	addData : function (result)
	{
		var perPage = 20;
		var count = Object.keys(result).length;
		var lastPage = Math.ceil(count/perPage);
		var returnData = new Array();

		for (var i = 0; i < lastPage; i++) {
			var resultsPerPage = new Array();
			for(var j= 0 ; j < perPage && j < count; j++)
			{
				resultsPerPage.push( result[j + perPage*i] );
			}
			var page = {
				"total" : count,
				"perPage" : perPage,
				"currentPage" : i+1,
				"lastPage" : lastPage,
				"data" : resultsPerPage
			};

			returnData.push(page);
		}

		if(count==0)
		{
			var resultsPerPage = new Array();
			var page = {
				"total" : count,
				"perPage" : perPage,
				"currentPage" : 0,
				"lastPage" : lastPage,
				"data" : resultsPerPage
			};
			returnData.push(page);
			JSON.stringify(returnData[0]);
		}

		return JSON.stringify(returnData);
	}

} ;
