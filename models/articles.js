var connection = require('../connection');
var utils = require('./utils');

function Article() {
 

 this.get = function(id, res) {
  connection.acquire(function(err, con) {
    con.query('select id,name,description from articles where idUser = ?', [id], function(err, result) {
      con.release();
      if (err) {
        res.send({status: 1, message: 'Failed to Query'});
      } else {
        var jsonString = utils.addData(result);
        res.send(JSON.parse(jsonString));
      }
    });
  });
};


this.create = function(article, res) {
  connection.acquire(function(err, con) {
    con.query('insert into articles set ?', article, function(err, result) {
      con.release();
      if (err) {
        res.send({status: 1, message: 'Article creation failed'});
      } else {
        res.send({status: 0, message: 'Article created successfully'});
      }
    });
  });
};

}
module.exports = new Article();

/*
 this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from users', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };
  */

